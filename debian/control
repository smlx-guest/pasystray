Source: pasystray
Section: sound
Priority: optional
Maintainer: Scott Leggett <scott@sl.id.au>
Build-Depends:
 debhelper (>= 11.0.0),
 libavahi-client-dev,
 libavahi-glib-dev,
 libayatana-appindicator3-dev,
 libgtk-3-dev,
 libnotify-dev,
 libpulse-dev,
 pkg-config
Standards-Version: 4.3.0
Homepage: https://github.com/christophgysin/pasystray
Vcs-Git: https://salsa.debian.org/smlx-guest/pasystray.git
Vcs-Browser: https://salsa.debian.org/smlx-guest/pasystray

Package: pasystray
Architecture: any
Depends:
 adwaita-icon-theme,
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 paman,
 paprefs,
 pavucontrol,
 pavumeter,
 pulseaudio-module-zeroconf
Description: PulseAudio controller for the system tray
 Pasystray enables control of various PulseAudio server settings from the X11
 system tray. It can:
    * adjust the volume of streams and sinks/sources
    * transfer streams between sinks/sources
    * switch the default sink/source
    * set the default server (PULSE_SERVER)
    * detect network PulseAudio services
    * rename devices
 .
 The commands associated with the suggested dependencies (paman, paprefs,
 pavucontrol, and pavumeter) may be launched from the menu provided by
 pasystray. If a command is not available, that particular menu item is
 disabled. pulseaudio-module-zeroconf enables detection and management of
 network PulseAudio services.
